#include <iostream>

const int N = 10; // ������ �������� ��� N

void printNumbers(int n, bool isEven) {
    std::cout << "Numbers from 0 to " << n << " that are ";

    if (isEven) {
        std::cout << "even: ";
        for (int i = 0; i <= n; i += 2) {
            std::cout << i << " ";
        }
    }
    else {
        std::cout << "odd: ";
        for (int i = 1; i <= n; i += 2) {
            std::cout << i << " ";
        }
    }

    std::cout << std::endl;
}

int main() {
    // ������� ��� ������ ����� �� 0 �� N
    std::cout << "Even numbers from 0 to " << N << ": ";
    for (int i = 0; i <= N; i += 2) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    // ������� ��� �������� ����� �� 0 �� N
    std::cout << "Odd numbers from 0 to " << N << ": ";
    for (int i = 1; i <= N; i += 2) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    // ���������� ������� ��� ������ ������ ����� �� 0 �� N
    printNumbers(N, true);

    // ���������� ������� ��� ������ �������� ����� �� 0 �� N
    printNumbers(N, false);

    return 0;
}
